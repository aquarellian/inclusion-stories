package com.bjss.inclusion.polymorphism;

public class TaskEstimator  {
    Man man;

    public TaskEstimator(Man man) {
        this.man = man;
    }

    public Integer estimate(Task task){
        return man.estimateTask(task);
    }
}

interface Task {
    Integer getComplexity();
}

interface Person {
    int estimateTask(Task task);
}

abstract class Woman extends AbstractPerson {
}

abstract class Man extends AbstractPerson {
}

class AbstractPerson implements Person {
    AbstractPerson() {
        this(MaturityLevel.REGULAR);
    }

    AbstractPerson(MaturityLevel maturityLevel) {
        this.maturityLevel = maturityLevel;
    }

    private MaturityLevel maturityLevel;

    @Override
    public int estimateTask(Task task) {
        return maturityLevel.estimateTask(task);
    }
}

enum MaturityLevel {
    JUNIOR,
    REGULAR,
    SENIOR,
    NINJA;

    public int estimateTask(Task task) {
        return task.getComplexity() / (ordinal() + 1);
    }

}





