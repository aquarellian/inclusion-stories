

### Polymorphism Story - Use of gender (and other identity) neutral terms (such as man day)

** When in doubt **

<quiz>
<question> Does estimation request explicitly specifies sexual identity of the subject? <question>
<answer correct> No, any skilled person will suffice <answer>
<answer> Yes, it's a work primarily related to sexual identity <answer>

<question> Does replacement of the person-specifying word with the opposite meaning makes it ridiculous? <question>
<answer correct> No, *woman-day* sounds ridiculous to my male colleagues, so is *man-day* to my female collegues <answer>
<answer correct> Yes, *businesswoman* sounds as good as *businessman* (though why using gender specific word if there's no restriction) <answer>
<quiz>

** What's the harm **

Even when a person from underrepresented category understands the intended meaning, it still requires additional processing: "They said "men"... No, they don't mean they **only** need men, they meant "person", so I fall under this category, too..." Each such interaction is not a big deal, but it triggers a micro-process in the mind of verbally omitted person making them question whether they are valuable enough in the given context.
Basically, it's a built-upon library based on reflection API, that analyses the use and checks whether current implementation has corresponding method and if it does builds an adapter to link existing method with a non-related interface used.
Does the above sound efficient?




